��          D      l       �   -   �      �      �   +   �     �   7   �     4     8  !   ?                          Attendance has been taken for this timeframe. Hour Minutes Take missing attendance for this timeframe. Project-Id-Version: Automatic Attendance plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:01+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Prisotnost je bila zabeležena v tem časovnem obdobju. Ura Minute Popravite manjkajočo prisotnost. 