��    	      d      �       �   -   �        B   $     g          �     �  +   �  #  �  2   �     '  <   =     z     �     �     �  3   �               	                               Attendance has been taken for this timeframe. Automatic Attendance Automatically take missing attendance for the day after this hour. Default Attendance Code Hour Minutes Take missing attendance Take missing attendance for this timeframe. Project-Id-Version: Automatic Attendance plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:01+0200
Last-Translator: Emerson Barros
Language-Team: RosarioSIS <info@rosariosis.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-Bookmarks: -1,-1,3,-1,-1,-1,-1,-1,-1,-1
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 A presença foi feito para este período de tempo. Presença automática Automaticamente considera faltas para o dia após esta hora. Código de presença padrão Hora Minutos Tomar presença faltante Considere faltas de frequência para este período. 