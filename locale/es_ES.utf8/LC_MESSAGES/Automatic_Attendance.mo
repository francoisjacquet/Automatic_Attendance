��    	      d      �       �   -   �        B   $     g          �     �  +   �  =  �  2        A  b   X  !   �     �     �  '   �  =                  	                               Attendance has been taken for this timeframe. Automatic Attendance Automatically take missing attendance for the day after this hour. Default Attendance Code Hour Minutes Take missing attendance Take missing attendance for this timeframe. Project-Id-Version: Automatic Attendance plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:00+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-Bookmarks: -1,-1,3,-1,-1,-1,-1,-1,-1,-1
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 La Asistencia ha sido tomada para este transcurso. Asistencia Automática Tomar los datos faltantes de asistencia de manera automática para el día, después de esta hora. Código de Asistencia por defecto Hora Minutos Tomar los datos faltantes de asistencia Tomar los datos faltantes de asistencia para este transcurso. 